<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ohm's Law</title>
</head>
<body>
	<form method="POST" id="ohmForm">
	    
	    Voltage(V):
		<input type="number" step="any" name="voltage" id="voltage"> <br> <br>
		Power(P):
		<input type="number"  name="power" id="power"> <br> <br>
		Current(I):
		<input type="number"  name="current" id="current"> <br> <br>
		Resistance(R): 
		<input type="number" name="resistance" id="resistance"> <br> <br>
		<input type="button" name="calculate" value="Calculate" id="calculate">
		<input type="button" name="clear" value="clear" id="clear">
		
	    <p id="ohm"> </p>
	    <p id="ohm2"> </p>
	    <p id="pow"> </p>
	     <p id="cur"> </p>
	    <p id="res"> </p>
	     <p id="volt"> </p>
	  
	    
	    <script
		  src="https://code.jquery.com/jquery-1.12.4.min.js"
		  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
		  crossorigin="anonymous"></script>	
		  	
		<script>
		
			$("#calculate").click(function(){
				var voltage= $("#voltage").val();
				var power= $("#power").val();
				var current= $("#current").val();
				var resistance= $("#resistance").val();
				var v = document.getElementById('voltage').value;
				var p = document.getElementById('power').value;
				var i = document.getElementById('current').value;
				var r = document.getElementById('resistance').value;
				
				if(v!=null && i!=null && p==="" && r===""){
					
						p= v*i; 
						r= v/i; 
						//document.getElementById("pow").innerHTML = p.toFixed(2);
						//document.getElementById("res").innerHTML = r.toFixed(2);
						document.getElementById("power").value = p;
						document.getElementById("resistance").value = r;
						//document.getElementById("ohm").innerHTML = "";
				}			
		   		
						
				else if(v!=null && r!=null && i==="" && p==="")
				{
					i= v/r;
					p= Math.pow(v, 2)/r;
					//document.getElementById("cur").innerHTML = i.toFixed(2);
					//document.getElementById("pow").innerHTML = p.toFixed(2);
					document.getElementById("current").value = i;
					document.getElementById("power").value = p;
					//document.getElementById("ohm").innerHTML = "";
				 }	
					
			  
				else if(p!=null && v!=null && i==="" && r==="")
				{
					
					i= p/v;
					r= Math.pow(v,2)/p;
				
					document.getElementById("current").value = i;
					document.getElementById("resistance").value = r;
					document.getElementById("ohm").innerHTML = "";
				}
				
				else if(i!=null && r!=null && v==="" && p==="")
				{
					v= i*r;
					p=Math.pow(i, 2) * r;
					document.getElementById("voltage").value = v;
					document.getElementById("power").value = r;
					document.getElementById("ohm").innerHTML = "";
				}
				else if(p!=null && i!=null && v==="" && r==="")
				{
					
					v=p/i;
					r=p/Math.pow(i,2);
					
					document.getElementById("voltage").value = v;
					document.getElementById("resistance").value = r;
					document.getElementById("ohm").innerHTML = "";
				}
				
				else if(p!=null && v!=null && i==="" && v==="")
				{
					i=Math.sqrt(p/r);
					v=Math.sqrt(p*r);
					
					document.getElementById("current").value = i;
					document.getElementById("voltage").value = v;
					document.getElementById("ohm").innerHTML = "";
					
				}
				/*else if(p!=null && v=="" && i=="" && r=="")
				{
					document.getElementById("ohm2").innerHTML = "Missing One Input";
					
				}
				/*else if(p==="" && v!=null && i==="" && r==="")
				{
					document.getElementById("ohm").innerHTML = "Missing One Input";
				}
				else if(p==="" && v==="" && i!=null && r==="")
				{
					document.getElementById("ohm").innerHTML = "Missing One Input";
				}
				else if(p==="" && v==="" && i==="" && r!=null)
				{
					document.getElementById("ohm").innerHTML = "Missing One Input";
				}
				*/
				else 
				{
				document.getElementById("ohm").innerHTML = "Only 2 inputs needed";
				
				}
				
				
			
			});
			$("#clear").click(function(){
			document.getElementById("ohmForm").reset();
			});
			

		</script> 
 
</body>
</html>